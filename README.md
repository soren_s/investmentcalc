**Information**  
This is a little program I made to learn C++ and to calculate values for my personal finance.
The compiled binary (a.out) is made to run on 64bit Linux distributions. If you want to use the program on a different platform, compile from source.
The tax section is based on the Swedish tax system, and thus not applicable for any other country. Put the tax input to 0 to calculate brutto values or change the source code to work with your own tax system.
As the code is percentage based it'll work with any currency.  
Contact: **development@soren.business**
#  
#  
**Using the program**  
The input values can both be decimal numbers or integers. The inputs, in order, are as follows:
Initial investment, Additional monthly investment, Amount of years you want to invest for, Expected average annual return (percentage), schablon value (for tax calculations), Annual fees (percentage of your total investment value), Expected annual raise, Expected average inflation
Set any of these inputs to 0 for it to be ignored in the calculation.
#  
**Usecase example:**  
![alt text](https://i.imgur.com/5MDDoFs.png "Example image 1")