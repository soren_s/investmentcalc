//Version: 1.3.2
//Author: Soren S. development@soren.business

//TODO: Add arguments instead of having to remember the order of the inputs, space at every third number

#include <iostream>
#include <math.h>
#include <string>
#include <iomanip>
using namespace std;

int main(int argc, char* argv[]) {
    cout << fixed;
    cout << setprecision(6);

    double initial = std::stod(argv[1]);
    double monthlyInvestment = std::stod(argv[2]);
    double years = std::stod(argv[3]);
    double percentage = std::stod(argv[4]);
    double schablon = std::stod(argv[5]);
    double fees = std::stod(argv[6])/12.0;
    double raise = (std::stod(argv[7])/100.0)+1.0;
    double inflation = std::stod(argv[8]);

    double monthlyPercentage = pow(((percentage / 100) + 1), (1.0 / 12.0)); //12th root of the percentage to get monthly percentage.
    double quarterlySchablon[4]; //stores the schablon count values for each quarter
    double calc = initial;
    double count = initial;
    double taxPaid = 0;
    double feesPaid = 0;
    double fee = 0;
    double raiseCount = 1.0;

    for (short y = years; y > 0; y--) {
        for (short q = 0; q < 4; q++) {
            quarterlySchablon[q] = calc; //Must be before the monthly calculations, see schablon source: https://www.skatteverket.se/privat/skatter/vardepapper/investeringssparkontoisk.4.5fc8c94513259a4ba1d800037851.html
            
            for (short m = 0; m < 3; m++) {
                calc *= monthlyPercentage;
                fee = (1.0 - (fees / 100.0));
                feesPaid += calc - (fee * calc); //Is technically pulled daily but monthly is enough for now
                calc *= fee;
                calc += monthlyInvestment;
                count += monthlyInvestment;
            }
        }

        double tax = (((quarterlySchablon[0] + quarterlySchablon[1] + quarterlySchablon[2] + quarterlySchablon[3] + (monthlyInvestment * 12.0)) / 4.0) * (schablon / 100.0)) * 0.3; //done at the end of each year
        taxPaid += tax;
        calc -= tax;
        monthlyInvestment *= raise;
        raiseCount *= raise;
    }

    double profit = calc - count;

    double monthlyReturn = (monthlyPercentage - 1) * 100;
    double annualRaise = (raise - 1) * 100;
    double totalRaise = (raiseCount - 1) * 100;
    double adjusted = calc * pow((1.0 - (inflation / 100.0)), years);

	cout << "Total: " << calc  << "\nReturn: " << profit << "\nSaved: " << count << "\n------------------------------" 
        << "\nTax paid: " << taxPaid << "\nFees paid: " << feesPaid << "\n------------------------------" 
        << "\nYearly return: " << percentage << "%" << "\nMonthly return: " << monthlyReturn << "%" << "\n------------------------------" 
        << "\nAnnual raise: " << annualRaise << "%" << "\nTotal raise: " << totalRaise << "%" << "\nInflation: " << inflation << "%" << "\nAdjusted for inflation: " << adjusted << endl;
	return 0;
}